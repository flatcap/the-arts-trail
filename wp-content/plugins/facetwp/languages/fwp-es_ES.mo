��    z      �  �   �      H
     I
  
   R
     ]
     e
     t
     x
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
               #     ,     5     D     J     ^  
   e     p     w     �     �     �  	   �  2   �  %   �            	        %     .     7      I  %   j     �     �     �     �     �     �     �     �     �     �     �     �               %     2     L     Q     ]     x     �  <   �     �  	   �     �  
   �  	                       0     @  	   N     X     a     g     t     {     �     �  
   �     �     �  ,   �     �     �          
  
             -     2     ?     F  
   N  	   Y  )   c  *   �  (   �  $   �  +        2  	   D     N     Z     f     o  
   v     �     �     �  :   �     �     �                 �       �     �     �     �  	             -     4     C  
   I     T     [     q     �     �     �     �     �     �               %     7     C     ]     c     w     �     �     �     �     �     �  
   �  :   �  )        5  	   >  
   H     S     \     h  %   }  2   �     �     �     �     �                      #        ?     B     [     n     ~  
   �  )   �     �     �      �            N   =     �     �     �     �     �     �     �     �          %     <     K  	   \     f  	   v     �     �     �     �     �     �  8   �               &     4     @     N     f     k     �     �     �  
   �  ?   �  <   �  '   $  *   L  $   w     �     �     �     �     �  
   �     �  
   �             =   (  !   f     �     �     �     �        <   P   F      f   0   T       4      M   d          s       j                     X   !   b   V   A            _       H   O      ,           e       G       U   E   1       9   -   q       m      &   7       k       =   i       >   h   w   x   #   /   B   ^   t   I           l             `   (               C   +   z          p            ?   :           6       J   [       v                  .   $      D                 W   r          y   g                 ;       	   %   o      Z       5       2   Y          *       
   @   c      '   )   u   "   8   3   L   ]       K   n      \   R       S   N   Q   a    Activate Activating Add New All post types Any Autocomplete Back Behavior Both Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Decimal Default Default label Display Code Display Value Dropdown End Date Enter keywords Error Error: invalid JSON Export Facet type Facets Fields to show Format GET variables Hierarchical Hierarchy How should multiple selections affect the results? How should permalinks be constructed? Import Imported Importing In Stock Indexing Indexing complete Is this a hierarchical taxonomy? Keep ghost choices in the same order? Label Less License Key License active Max Min More Name Narrow the result set No Not yet activated Nothing to import Number Range Open query builder Out of Stock Overwrite existing items? Page Parent term Paste the import code here Permalink Type Placeholder text Please activate or renew your license for automatic updates. Post Author Post Date Post Modified Post Title Post Type Posts Prefix Preserve ghost order Query Arguments Query Builder Raw Value Re-index Reset Save Changes Saving Search Search engine Send to editor Separators Settings Settings saved Show choices that would return zero results? Show ghosts Skipped Slider Sort by Start Date Start typing... Step Stock Status Suffix Support Taxonomies Templates Text that appears after each slider value Text that appears before each slider value The amount of increase between intervals The data used to populate this facet The maximum number of facet choices to show The number format Thousands Title (A-Z) Title (Z-A) URL Hash Update WP Default Welcome Welcome to FacetWP What goes here? Which posts would you like to use for the content listing? Widen the result set WooCommerce Yes expires of Project-Id-Version: FacetWP
POT-Creation-Date: 2015-08-03 17:05-0400
PO-Revision-Date: 2015-08-03 17:06-0400
Last-Translator: Matt Gibbs <hello@facetwp.com>
Language-Team: Matt Gibbs <hello@facetwp.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.3
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Activar Activación Añadir nuevo Todos los tipos de post Cualquier Autocompletar Atrás Comportamiento Ambos Checkboxes Cuenta Campos personalizados Origen de los datos Fecha (más recientes) Fecha (más antiguo) Rango de datos Sistema de numeración decimal Por omisión Etiqueta predeterminada Mostrar código Muestra el valor Lista desplegable Fecha final Introducir palabras clave Error Error: invalid JSON Exportar Tipo de faceta Faceta Campos Formato Variables tipo GET Jerarquizada Jerarquía ¿Cómo debe afectar el resultado la selección múltiple? ¿Cómo deben construirse los permalinks? Importar Importado Importando En Stock Indización Indexación completa ¿Esto es una taxonomía jerárquica? ¿Mantener opciones de fantasma en el mismo orden? Etiqueta Menos Llave de licencia Licencia activa Max Min Más Nombre Restringe el conjunto de resultados No No ha sido activado aún Nada para importar Rango numérico Generador de consultas abiertas ¡Agotado! ¿Sobreescribir los elementos existentes? Página Término superior Pegar el código importado aquí Tipo Permalink Texto del marcador de posición Por favor activar o renovar su licencia para las actualizaciones automáticas. Autor de entrada Fecha de entrada Entrada modificada Título de Post Tipo de entrada Entradas Prefijo Preservar el orden fantasma Argumentos de consulta Generador de consultas Valor primario Reindexar correo Restaurar Guardar cambios Guardando Buscar Buscador Enviar al editor Separadores Configuraciones Configuraciones guardadas ¿Mostrar las opciones que le devuelven cero resultados? Ver fantasmas Saltado Presentación Ordenar por Fecha inicial Escriba su búsqueda... Paso Estado del inventario Sufijo Soporte Taxonomías Plantillas Texto que aparece después de cada valor del control deslizante Texto que aparece antes de cada valor del control deslizante La cantidad de aumento entre intervalos Datos utilizados para publicar esta faceta Máximo número de facetas a mostrar Formato de número Miles Título (A-Z) Título (Z-A) Hash URL Actualizar WP por omisión Bienvenido Bienvenido a FacetWP ¿Qué pasa aquí? ¿Qué mensajes desea utilizar para el listado de contenidos? Amplía el conjunto de resultados Tienda Si expira de 