��    {      �  �   �      h
     i
  
   r
     }
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
  
   �
     
               (     5     C     L     U     d     j     ~  
   �     �     �     �     �     �  	   �     �  2   �  %        9     @  	   I     S     \     e      w  %   �     �     �     �     �     �     �     �     �     �               !     3     @     S     `     z          �     �     �  <   �       	          
   '  	   2     <     B     I     ^     n  	   |     �     �     �     �     �     �     �  
   �     �     �  ,   �          )     1     8  
   @     K     [     `     m     t  
   |  	   �  )   �  *   �  (   �  $     +   4     `  	   r     |     �     �     �  
   �     �     �     �  :   �          *     6     :     B  �  E     	               +  	   D     N     `     i     w     �  	   �     �     �     �     �     �     �     �     �       	   !     +     4     9     Q     X     p     x     �     �     �     �  
   �  	   �     �  3   �  +        7  	   ?     I     V     b     q      �  1   �  	   �     �     �     �                      "   "     E     H     \     y     �     �  %   �     �     �  (   �          '  M   8     �     �  
   �     �  	   �     �     �  &   �     �     �                    %     5     A     I     [  
   p     {     �  &   �     �     �     �  
   �     �     �               +     4  
   =     H  0   Q  7   �  ,   �  '   �  ,        <     R     [     h     u     ~     �  	   �     �     �  8   �     �          *     .     4         k   w       H   ]   f   3   g             F   q   U   P   E       z           M   8                  B   (   	   >   d   #   T   [   -   2   D   Q   =      ;   5   x           1   V   t   X   +   `   K      m       )             ?   y           A   7       %       N   C   /   &   l      9   b       c      e                j       *       s   ^      h   $   '   Y   0             G       a          p       @   W       S   
       ,                  v           .         i   4   \   O       {       :   "   o                        6   r             _              u      n       I      L   <   !   Z   J      R                    Activate Activating Add New All post types Any Autocomplete Back Behavior Both Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Decimal Default Default label Display Code Display Value Dropdown End Date Enter keywords Error Error: invalid JSON Export Facet type Facets Fields to show Format GET variables Hierarchical Hierarchy Highest Count How should multiple selections affect the results? How should permalinks be constructed? Import Imported Importing In Stock Indexing Indexing complete Is this a hierarchical taxonomy? Keep ghost choices in the same order? Label Less License Key License active Max Min More Name Narrow the result set No Not yet activated Nothing to import Number Range Open query builder Out of Stock Overwrite existing items? Page Parent term Paste the import code here Permalink Type Placeholder text Please activate or renew your license for automatic updates. Post Author Post Date Post Modified Post Title Post Type Posts Prefix Preserve ghost order Query Arguments Query Builder Raw Value Re-index Reset Save Changes Saving Search Search engine Send to editor Separators Settings Settings saved Show choices that would return zero results? Show ghosts Skipped Slider Sort by Start Date Start typing... Step Stock Status Suffix Support Taxonomies Templates Text that appears after each slider value Text that appears before each slider value The amount of increase between intervals The data used to populate this facet The maximum number of facet choices to show The number format Thousands Title (A-Z) Title (Z-A) URL Hash Update WP Default Welcome Welcome to FacetWP What goes here? Which posts would you like to use for the content listing? Widen the result set WooCommerce Yes expires of Project-Id-Version: FacetWP
POT-Creation-Date: 2015-08-03 17:03-0400
PO-Revision-Date: 2015-08-03 17:04-0400
Last-Translator: Matt Gibbs <hello@facetwp.com>
Language-Team: Matt Gibbs <hello@facetwp.com>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.3
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Attiva Attivazione Aggiungi nuovo Tutti i tipi di messaggi Qualsiasi Autocompletamento Indietro Comportamento Entrambi Checkbox Conteggio Custom fields Sorgente dei dati Data (Più recenti) Data (Meno recenti) Range di Date Decimale Default Etichetta di default Visualizza codice Etichetta Dropdown Fine Inserisci parole chiave Errore Errore: non valido JSON Esporta Tipo di Facet Facet Campi da mostrare Formato Variabili GET Gerarchico Gerarchia Massima Conte Come influenzano i risultati le selezioni multiple? Come devono essere strutturati i permalink? Importa Importato Importazione Disponibile Indicizzazione L’indicizzazione completa Questa tassonomia è gerarchica? Mantenere gli elementi vuoti nello stesso ordine? Etichetta Di meno License Key Licenza attiva Max Min Di più Nome Restringere il numero di risultati No Non ancora attivata Nessun elemento da importare Range numerico Generatore di query aperta Esaurito Sovrascrivere gli elementi esistenti? Pagina Termine padre Incolla qui il codice per l'importazione Tipologia di Permalink Testo riempitivo Si prega di attivare o rinnovare la licenza per gli aggiornamenti automatici. Autore Data Modificato Titolo Post Type Post Prefisso Mantieni l'ordine degli elementi vuoti Parametri della query Generatore di query Slug Re-indicizza Reset Salva modifiche Salvataggio Ricerca Motore di ricerca Inviare all’editor Separatori Impostazioni Impostazioni salvate Mostrare gli elementi senza risultati? Mostra elementi vuoti Tralasciato Slider Ordina per Inizio Inizia a scrivere... Step Archivio di stato Suffisso Supporto Tassonomie Template Testo che appare dopo ogni elemento dello slider Testo che appare prima di ciascun elemento dello slider L'entità dell'incremento fra gli intervalli I dati utilizzati per popolare il facet Il numero massimo di opzioni da visualizzare Il formato del numero Migliaia Titolo (A-Z) Titolo (Z-A) URL Hash Aggiorna Default di WP Benvenuto Benvenuto a FacetWP Cosa succede qui? Quali messaggi vuoi utilizzare per il profilo contenuto? Ampliare il numero di risultati WooCommerce Sì scade di 