��    {      �  �   �      h
     i
  
   r
     }
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
  
   �
     
               (     5     C     L     U     d     j     ~  
   �     �     �     �     �     �  	   �     �  2   �  %        9     @  	   I     S     \     e      w  %   �     �     �     �     �     �     �     �     �     �               !     3     @     S     `     z          �     �     �  <   �       	          
   '  	   2     <     B     I     ^     n  	   |     �     �     �     �     �     �     �  
   �     �     �  ,   �          )     1     8  
   @     K     [     `     m     t  
   |  	   �  )   �  *   �  (   �  $     +   4     `  	   r     |     �     �     �  
   �     �     �     �  :   �          *     6     :     B  K  E  
   �     �     �     �     �     �     �  	                  0     7     Q     ]     t     �     �     �     �  '   �     �     �     �               %     =     I     V     _     s     z     �  
   �     �  ;   �  .   �       
   )     4  	   @     J     V  0   p  <   �     �     �     �                         "     '     A     F  !   [     }     �     �  "   �     �  !   �  #        (     6  M   G     �     �     �     �     �     �     �     �          %     7     <     K     Y     o     w     }     �  
   �     �  %   �  B   �          +     9     @     O     \     p     x     �     �  	   �  	   �  -   �      �  :   �  5   3  )   i     �     �     �     �     �     �     �  
   �     �       ?        \     u     �     �     �         k   w       H   ]   f   3   g             F   q   U   P   E       z           M   8                  B   (   	   >   d   #   T   [   -   2   D   Q   =      ;   5   x           1   V   t   X   +   `   K      m       )             ?   y           A   7       %       N   C   /   &   l      9   b       c      e                j       *       s   ^      h   $   '   Y   0             G       a          p       @   W       S   
       ,                  v           .         i   4   \   O       {       :   "   o                        6   r             _              u      n       I      L   <   !   Z   J      R                    Activate Activating Add New All post types Any Autocomplete Back Behavior Both Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Decimal Default Default label Display Code Display Value Dropdown End Date Enter keywords Error Error: invalid JSON Export Facet type Facets Fields to show Format GET variables Hierarchical Hierarchy Highest Count How should multiple selections affect the results? How should permalinks be constructed? Import Imported Importing In Stock Indexing Indexing complete Is this a hierarchical taxonomy? Keep ghost choices in the same order? Label Less License Key License active Max Min More Name Narrow the result set No Not yet activated Nothing to import Number Range Open query builder Out of Stock Overwrite existing items? Page Parent term Paste the import code here Permalink Type Placeholder text Please activate or renew your license for automatic updates. Post Author Post Date Post Modified Post Title Post Type Posts Prefix Preserve ghost order Query Arguments Query Builder Raw Value Re-index Reset Save Changes Saving Search Search engine Send to editor Separators Settings Settings saved Show choices that would return zero results? Show ghosts Skipped Slider Sort by Start Date Start typing... Step Stock Status Suffix Support Taxonomies Templates Text that appears after each slider value Text that appears before each slider value The amount of increase between intervals The data used to populate this facet The maximum number of facet choices to show The number format Thousands Title (A-Z) Title (Z-A) URL Hash Update WP Default Welcome Welcome to FacetWP What goes here? Which posts would you like to use for the content listing? Widen the result set WooCommerce Yes expires of Project-Id-Version: FacetWP
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-08-03 17:06-0400
PO-Revision-Date: 2015-08-03 17:08-0400
Last-Translator: Matt Gibbs <hello@facetwp.com>
Language-Team: Marie Hoppe <info@mk-hoppe.de>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: de_DE
X-Generator: Poedit 1.8.3
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Loco-Parser: loco_parse_po
X-Loco-Target-Locale: en_GB
X-Poedit-SearchPath-0: .
 aktivieren Aktivierung Neuer hinzufügen Alle verfügbaren Post Types Alle Automatisch vervollständigen Zurück Verhalten Beides Kontrollkästchen (Checkbox) Anzahl Benutzerdefinierte Felder Datenquelle Datum (Neueste zuerst) Datum (Älteste zuerst) Zeitraum Dezimal Standard Standardbeschriftung Code zur Darstellung der Suchergebnisse Anzeigewert Dropdown Enddatum Stichwörter eingeben Fehlermeldung Fehler: Ungültige JSON Exportieren Facetten-Typ Facetten Anzuzeigende Felder Format GET Variablen Hierarchisch Hierarchie Höchster Zähler Wie soll sich Mehrfachauswahl auf die Ergebnisse auswirken? Wie sollten die Permalinks werden konstruiert? Importieren Importiert Importieren Auf Lager Indizierung Vollständige Indizierung Handelt es sich um eine hierarchische Taxonomie? Halten Sie Ghost-Entscheidungen in der gleichen Reihenfolge? Beschriftung weniger Lizenzschlüssel Lizenz ist aktiv max min mehr Name Suchergebnisse verfeinern Nein Noch nicht aktiviert Keine Daten für Import gefunden. Zahlenbereich Geöffneten Abfrage-Generator Nicht auf Lager Bestehende Objekte überschreiben? Seite Übergeordneter Taxonomie-Begriff Fügen Sie den Import-Code hier ein Permalink-Typ Platzhalter-Text Bitte aktivieren Sie oder erneuern Sie Ihre Lizenz für automatische Updates. Beitragsautor Veröffentlichungsdatum Letzte Aktualisierung Beitragstitel Post Typ Posts Präfix Ghost Reihenfolge beibehalten Abfrageparameter Abfrage-Generator Wert Neu indexieren Zurücksetzen Änderungen speichern Sichern Suche Such-Engine An Editor senden Abscheider Einstellungen Die Einstellungen wurden gespeichert. Auswahlmöglichkeiten anzeigen, die keine Treffer ergeben würden? Leere anzeigen Übersprungen Slider Sortieren nach Anfangsdatum Eingabe beginnen... Schritt Lagerbestand Suffix Stützen Taxonomie Templates Nach jedem Schiebereglerwert angezeigten Text Präfix für jedes Slider-Object Intervallgröße (Menge, um die je Intervall erhöht wird) Welche Daten sind für diese Facette ausschlaggebend? Maximale Anzahl von Auswahlmöglichkeiten Zahlenformat Tausende Titel (A - Z) Titel (Z - A) URL-Hash Auswahl anwenden WP Standard Willkommen Willkommen bei FacetWP Was geht hier? Welche Beiträge möchten Sie die Inhalte Auflistung verwenden? Suchergebnisse erweitern WooCommerce Ja Läuft ab am von 