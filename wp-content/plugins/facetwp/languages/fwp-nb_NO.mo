��    {      �  �   �      h
     i
  
   r
     }
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
  
   �
     
               (     5     C     L     U     d     j     ~  
   �     �     �     �     �     �  	   �     �  2   �  %        9     @  	   I     S     \     e      w  %   �     �     �     �     �     �     �     �     �     �               !     3     @     S     `     z          �     �     �  <   �       	          
   '  	   2     <     B     I     ^     n  	   |     �     �     �     �     �     �     �  
   �     �     �  ,   �          )     1     8  
   @     K     [     `     m     t  
   |  	   �  )   �  *   �  (   �  $     +   4     `  	   r     |     �     �     �  
   �     �     �     �  :   �          *     6     :     B  �  E       
             (     ;     L     Y  
   a     l     r     �     �  	   �     �     �     �     �     �     �     �            	         *     >     C  	   V  	   `     j  
   s     ~     �  
   �     �     �  +   �  %   �     	  	     	     	   &     0     <  !   Q  &   s     �     �     �     �     �     �     �     �     �     �     �               +     C  $   R     w     |     �     �     �  G   �               ,     :     I     V     ^      f     �     �     �  
   �  	   �     �     �     �  
   �     �  
                (   5     ^     m     y     �  	   �     �     �     �     �     �     �     �  *   �  )        >  &   Y  %   �     �     �     �     �     �     �     �  	   �            5   (     ^     s          �     �         k   w       H   ]   f   3   g             F   q   U   P   E       z           M   8                  B   (   	   >   d   #   T   [   -   2   D   Q   =      ;   5   x           1   V   t   X   +   `   K      m       )             ?   y           A   7       %       N   C   /   &   l      9   b       c      e                j       *       s   ^      h   $   '   Y   0             G       a          p       @   W       S   
       ,                  v           .         i   4   \   O       {       :   "   o                        6   r             _              u      n       I      L   <   !   Z   J      R                    Activate Activating Add New All post types Any Autocomplete Back Behavior Both Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Decimal Default Default label Display Code Display Value Dropdown End Date Enter keywords Error Error: invalid JSON Export Facet type Facets Fields to show Format GET variables Hierarchical Hierarchy Highest Count How should multiple selections affect the results? How should permalinks be constructed? Import Imported Importing In Stock Indexing Indexing complete Is this a hierarchical taxonomy? Keep ghost choices in the same order? Label Less License Key License active Max Min More Name Narrow the result set No Not yet activated Nothing to import Number Range Open query builder Out of Stock Overwrite existing items? Page Parent term Paste the import code here Permalink Type Placeholder text Please activate or renew your license for automatic updates. Post Author Post Date Post Modified Post Title Post Type Posts Prefix Preserve ghost order Query Arguments Query Builder Raw Value Re-index Reset Save Changes Saving Search Search engine Send to editor Separators Settings Settings saved Show choices that would return zero results? Show ghosts Skipped Slider Sort by Start Date Start typing... Step Stock Status Suffix Support Taxonomies Templates Text that appears after each slider value Text that appears before each slider value The amount of increase between intervals The data used to populate this facet The maximum number of facet choices to show The number format Thousands Title (A-Z) Title (Z-A) URL Hash Update WP Default Welcome Welcome to FacetWP What goes here? Which posts would you like to use for the content listing? Widen the result set WooCommerce Yes expires of Project-Id-Version: FacetWP
POT-Creation-Date: 2015-08-03 17:02-0400
PO-Revision-Date: 2015-08-03 17:02-0400
Last-Translator: Matt Gibbs <hello@facetwp.com>
Language-Team: Matt Gibbs <hello@facetwp.com>
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.3
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 Aktiver Aktivering Lag Nytt Alle innlegg typer hvilke som helst Autofullfør Tilbake Oppførsel Begge Avhukningsbokser Antall Egendefinerte felt Datakilde Dato (nyeste) Dato (eldste) Datoområde Titallsystemet Standard Standardetikett Visningskode Visningsverdi Nedtrekksmeny Sluttdato Legg inn nøkkelord Feil Feil: Ugyldig JSON Eksporter Fasettype Fasetter Vis felter Format GET-variabler Hierarkisk Hierarki Høyeste antall Hvordan skal flervalg påvirke resultatene? Hvordan skal permalenker konstrueres? Importer Importert Importere På lager Indeksering Indeksering komplett Er dette en hierarkisk taksonomi? Behold spøkelser i samme rekkefølge? Etikett Mindre Lisensnøkkel Aktiv lisens Maks Min Mer Navn Smalne inn resultatsettet Nei Ikke aktivert ennå Ingenting å importere Tallområde Åpne spørreverktøyet Ikke på lager Overskriv eksisterende oppføringer? Side Overordnet term Lim inn importkoden her Type permalenke Plassholdertekst Vennligst aktivere eller fornye lisensen for automatiske oppdateringer. Innleggsforfatter Innleggsdato Innegg endret Innleggstittel Innleggstype Innlegg Prefiks Bevar rekkefølge på spøkelser Spørringsargumenter Spørringsutformingen Råverdi Reindekser Nullstill Lagre endringer Lagrer Søk Søkemotor Sende å editor Skilletegn Innstillinger Lagret innstillinger Vis valg som returnerer null resultater? Vis spøkelser Hoppet over Glidebryter Sorter etter Startdato Begynn å skrive... Steg Lagerstatus Endelse Brukerstøtte Taksonomier Maler Tekst som vises før hver glidebryterverdi Test som vises før hver glidebryterverdi Økning mellom intervaller Date brukt til å fylle denne fasetten Maksimalt antall fasettvalg som vises Tallformatet Tusenvis Tittel (A–Å) Tittel (Å–A) URL-hash Oppdater WP-standard Velkommen Velkommen til FacetWP Hva går her? Hvilke innlegg du vil bruke for innhold oppføringen? Utvid resultatsettet WooCommerce Ja utløper av 