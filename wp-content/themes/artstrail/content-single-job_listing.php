<?php
/**
 * The template for displaying a single job listings' content.
 *
 * @package Listify
 */

global $job_manager;
?>




<?php
// START ALEX

// get taxonomies terms links
function current_taxonomy() {
    global $post, $post_id;
    // get post by post id
    $post = &get_post($post->ID);
    // get post type by post
    $post_type = $post->post_type;
    // get post type taxonomies
    $taxonomies = get_object_taxonomies($post_type);

    foreach ($taxonomies as $taxonomy) {
        $out = $taxonomy;
        // get the terms related to post
        $terms = get_the_terms( $post->ID, $taxonomy );
        if ( !empty( $terms ) ) {
            foreach ( $terms as $term )
                $out = $term->name;
        }
    }

    return $out;
}


$current_tax = current_taxonomy();
$artists = array("Artists", "Ceramic", "Glass", "Metal", "Painting", "Photography", "Stone", "Textile", "Wood");
$events = array("Events");
$venues = array("Venues", "Gallery", "Other Arts Venues", "Studio");

// Example Usage to Output
// if (in_array($current_tax, $artists)) {
//	echo '<h2>Artists</h2>';
//	}

// Example of fetching Post Meta
// $website = get_post_meta( $post->ID, '_company_website', true );
// if ( $website ) {
//     echo '<li>' . __( 'Website:' ) . ' ' . $website . '</li>';
// }




// END ALEX
?>



<div class="single_job_listing" itemscope itemtype="http://schema.org/LocalBusiness" <?php echo apply_filters( 'listify_job_listing_data', '' ); ?>>

	<div <?php echo apply_filters( 'listify_cover', 'listing-cover content-single-job_listing-hero', array( 'size' => 'full' ) ); ?>>

		<div class="content-single-job_listing-hero-wrapper cover-wrapper container">

			<div class="content-single-job_listing-hero-inner row">

				<div class="content-single-job_listing-hero-company col-md-6 col-sm-12">
					<?php do_action( 'listify_single_job_listing_meta' ); ?>
				</div>

				<div class="content-single-job_listing-hero-actions col-md-6 col-sm-12">

				</div>

			</div>

		</div>

	</div>


	<div id="primary" class="container">
		<div class="row content-area">


			<?php if ( listify_has_integration( 'woocommerce' ) ) : ?>
				<?php wc_print_notices(); ?>
			<?php endif; ?>

			<main id="main" class="site-main col-md-8 col-sm-7 col-xs-12" role="main">





					


				<!-- Gallery Images URLS with OWL Carousel and prettyPhoto -->
				<?php

						$gallery_images1 = get_post_meta( $post->ID, '_gallery_images1', true );
						$gallery_images2 = get_post_meta( $post->ID, '_gallery_images2', true );
						$gallery_images3 = get_post_meta( $post->ID, '_gallery_images3', true );
						$gallery_images4 = get_post_meta( $post->ID, '_gallery_images4', true );
						$gallery_images5 = get_post_meta( $post->ID, '_gallery_images5', true );
						$gallery_images6 = get_post_meta( $post->ID, '_gallery_images6', true );


						$large_image1 = job_manager_get_resized_image( $gallery_images1, 'large' );
						$large_image2 = job_manager_get_resized_image( $gallery_images2, 'large' );
						$large_image3 = job_manager_get_resized_image( $gallery_images3, 'large' );
						$large_image4 = job_manager_get_resized_image( $gallery_images4, 'large' );
						$large_image5 = job_manager_get_resized_image( $gallery_images5, 'large' );
						$large_image6 = job_manager_get_resized_image( $gallery_images6, 'large' );

						if ( $gallery_images1 ) { 
							echo '<aside class="widget widget-job_listing listify_widget_panel_listing_content">';
							echo '<div id="single-gallery">';
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image2 . '"><img src="' . $large_image1 . '" alt=""></a>'; 
						}
						if ( $gallery_images2 ) { 
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image2 . '"><img src="' . $large_image2 . '" alt=""></a>'; 
						}
						if ( $gallery_images3 ) { 
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image3 . '"><img src="' . $large_image3 . '" alt=""></a>'; 
						}
						if ( $gallery_images4 ) { 
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image4 . '"><img src="' . $large_image4 . '" alt=""></a>'; 
						}
						if ( $gallery_images5 ) { 
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image5 . '"><img src="' . $large_image5 . '" alt=""></a>'; 
						}
						if ( $gallery_images6 ) { 
							echo '<a rel="prettyPhoto[gallery]" href="' . $large_image6 . '"><img src="' . $large_image6 . '" alt=""></a>'; 
						}
						if ( $gallery_images1 ) { 
							echo '</div></aside>';
						}

						
					?>
				<!-- /Gallery Images URLS with OWL Carousel and prettyPhoto -->

				<!-- Main Description -->
				
					
					<?php $job_description = get_post_meta( $post->ID, '_job_description', true );
					if ( $job_description ) {
						echo '<aside class="widget widget-job_listing listify_widget_panel_listing_content">';
						echo '<h2 class="widget-title widget-title-job_listing ion-ios7-compose-outline">About</h2>';
					    echo '<p>' . $job_description . '</p>';
					    echo '</aside>';
					}
					?>
	
				<!-- /Main Description -->

				<!-- Latest News -->

					<?php $latest_news = get_post_meta( $post->ID, '_latest_news', true );
					if ( $latest_news ) {
						echo '<aside class="widget widget-job_listing listify_widget_panel_listing_content">';
						echo '<h2 class="widget-title widget-title-job_listing ion-ios7-compose-outline">Latest News</h2>';
					    echo $latest_news;
					    echo '</aside>';
					}
					?>
				
				<!-- /Latest News -->






				<?php do_action( 'single_job_listing_start' ); ?>

				<?php
					if ( ! dynamic_sidebar( 'single-job_listing-widget-area' ) ) {
						$defaults = array(
							'before_widget' => '<aside class="widget widget-job_listing">',
							'after_widget'  => '</aside>',
							'before_title'  => '<h2 class="widget-title widget-title-job_listing %s">',
							'after_title'   => '</h2>',
							'widget_id'     => ''
						);

						the_widget(
							'Listify_Widget_Listing_Video',
							array(
								'title' => __( 'Video', 'listify' ),
								'icon'  => 'ios7-film-outline',
							),
							wp_parse_args( array( 'before_widget' => '<aside class="widget widget-job_listing
							listify_widget_panel_listing_video">' ), $defaults )
						);

					}
				?>

				<!-- Artist Twitter Username -->
				<?php
				$twitter_user = get_post_meta( $post->ID, '_twitter_username', true );
				if($twitter_user) {
				    echo "<a class='twitter-timeline' href='https://twitter.com/" . $twitter_user . "' data-widget-id='580021867437813760'>Tweets by " . $getallmeta['_twitter_username'][0] . "</a>";
				}
		
				?>

				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>




				<?php do_action( 'single_job_listing_end' ); ?>

			</main>

			<?php get_sidebar( 'single-job_listing' ); ?>

		</div>
	</div>
</div>
