<?php
/**
 * Listify child theme.
 */

function listify_child_styles() {
    wp_enqueue_style( 'listify-child', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

/** Place any new code below this line */



/* Dequeue Styles from Parent Theme */
add_action( 'wp_enqueue_scripts', 'remove_default_stylesheet', 25 );

function remove_default_stylesheet() {
    wp_dequeue_style( 'listify' );
    wp_deregister_style( 'listify' );
}







// Set the listings to the name of the person / event or venue only without location
add_filter( 'submit_job_form_save_job_data', 'custom_submit_job_form_save_job_data', 10, 5 );
 
function custom_submit_job_form_save_job_data( $data, $post_title, $post_content, $status, $values ) {
 
    $job_slug   = array();
 
// Name
    if ( ! empty( $values['company']['company_name'] ) )
        $job_slug[] = $values['company']['company_name'];
 
// Prepend location
    if ( ! empty( $values['job']['best_location'] ) )
        $job_slug[] = $values['job']['best_location'];
 
    $job_slug[] = $post_title; 
    
    $data['post_name'] = implode( '-', $job_slug );
    
    return $data;
}


