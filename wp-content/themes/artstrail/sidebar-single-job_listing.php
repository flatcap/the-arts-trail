<?php
/**
 * The Sidebar for single listing items.
 *
 * @package Listify
 */

$defaults = array(
	'before_widget' => '<aside class="widget widget-job_listing">',
	'after_widget'  => '</aside>',
	'before_title'  => '<h1 class="widget-title widget-title-job_listing %s">',
	'after_title'   => '</h1>',
	'widget_id'     => ''
);
?>

	<!-- Get all meta data for my listing custom fields once so that I can query within this get request for lighter server load -->
	<?php $getallmeta=get_post_meta( $post->ID);?>




	<div id="secondary" class="widget-area col-md-4 col-sm-5 col-xs-12" role="complementary">

		<!-- Profile Image -->
		<?php
			$profileimage = get_post_meta( $post->ID, '_profile_image', true );
			$medium_profile = job_manager_get_resized_image( $profileimage, 'medium' );

			if ( $profileimage ) { 
				echo '<aside class="widget widget-job_listing profile-image"><img src="' . $medium_profile . '"/></aside>';
			}

		?>
		<!-- /Profile Image -->




<!-- contact info -->
<?php

	// if ( $getallmeta['_application'][0] ) {
    	// echo '<a href="mailto:' . $getallmeta['_application'][0] . '">' . $getallmeta['_application'][0] . '</a>';
    // }

?>

<?php  
	echo '<aside class="widget widget-author-info">';
	if ( $getallmeta['_job_title'][0] ) {
		echo '<h2>' . $getallmeta['_job_title'][0] . '</h2>';
	}
	if ( $getallmeta['_phone'][0] ) {
		echo '<span class="ion-phone"></span> Call <span>' . $getallmeta['_phone'][0] . '</span><br>';
	}   
	


	if ( $getallmeta['_application'][0] ) {
		echo '<a href="#" class="" data-toggle="modal" data-target="#listingModal">Email ' . $getallmeta['_job_title'][0] . '</a><br>';
		echo '<script>listing_email = "'. $getallmeta['_application'][0] . '"; jQuery( document ).ready(function() {jQuery("#input_1_7").val(listing_email);});</script>';
	}


	if ( $getallmeta['_company_website'][0] ) {
		echo '<a target="_blank" href="' . $getallmeta['_company_website'][0] . '">' . 'Visit Website' . '</a>';
	}

	if ( $getallmeta['_facebook_url'][0] or $getallmeta['_twitter_username'][0] ) {
		echo '<div class="floatsocial">';
	    if ( $getallmeta['_facebook_url'][0] ) {
	    	echo '<a class="facebook" href="' . $getallmeta['_facebook_url'][0] . '" title="Like us on Facebook" target="_blank"><i class="fa fa-facebook"></i></a>';
		}
	    if ( $getallmeta['_twitter_username'][0] ) {
	    	echo '<a class="twitter" href="http://twitter.com/' . $getallmeta['_twitter_username'][0] . '" title="Follow us on Twitter" target="_blank"><i class="fa fa-twitter"></i></a>';
		}
	    echo '</div>';
	}
	echo '</aside>';    



	

?>


<div id="listingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<span class="close" data-dismiss="modal" aria-hidden="true">close</span>
            	<h4 class="modal-title" id="myModalLabel">Book / Enquire</h4>
            </div>
            <div class="modal-body">
				<?php  echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" tabindex="20"]'); ?>
            </div>
    	</div>
    </div>
</div>
<!-- contact info -->






		<!-- Event Date -->
		<?php
		if ( $getallmeta['_event_date'][0])
			{ 
				echo '<aside class="widget widget-job_listing">';
				echo '<h3 class="widget-title widget-title-job_listing">Event Date</h3>';
				echo '<h3>' . $getallmeta['_event_date'][0] . '</h3>'; 
				echo '</aside>';
			}
		?>


		<?php if ( ! dynamic_sidebar( 'single-job_listing' ) ) : ?>
			<?php
				global $listify_strings;

				the_widget(
					'Listify_Widget_Listing_Author',
					array(
						'descriptor' => sprintf( __( '%s Owner', 'listify' ), $listify_strings->label( 'singular' ) ),
						'biography' => true
					),
					$defaults
				);

				the_widget(
					'Listify_Widget_Listing_Business_Hours',
					array(
						'title' => __( 'Hours of Operation', 'listify' ),
						'icon'  => 'clock'
					),
					wp_parse_args( array(
						'before_widget' => '<aside class="widget widget-job_listing listify_widget_panel_listing_business_hours">',
					), $defaults )
				);
			?>
		<?php endif; ?>











<!-- ALEX FOR SIDEBAR -->
					<?php if ( $getallmeta['_facebook_url'][0] or $getallmeta['_twitter_username'][0] ) {
					
						echo '<aside class="widget widget-job_listing">';

							if ( $getallmeta['_hour_information'][0] ) {
								echo '<h3 class="widget-title widget-title-job_listing">Opening Information</h3>';
						    	echo '<p>' . $getallmeta['_hour_information'][0] . '</p>';
						    }
						
						    if ( $getallmeta['_address_information'][0] ) {
								echo '<h3 class="widget-title widget-title-job_listing">Useful Information</h3>';
							    echo '<p>' . $getallmeta['_address_information'][0] . '</p>';
							}
					
						echo '</aside>';
					
					} //end if 				
					?>













			<?php
				// global $listify_strings;

				// the_widget(
				// 	'Listify_Widget_Listing_Business_Hours',
				// 	array(
				// 		'title' => __( 'Hours of Operation', 'listify' ),
				// 		'icon'  => 'clock'
				// 	),
				// 	wp_parse_args( array(
				// 		'before_widget' => '<aside class="widget widget-job_listing listify_widget_panel_listing_business_hours">',
				// 	), $defaults )
				// );
			?>












<!-- /ALEX FOR SIDEBAR -->


	</div><!-- #secondary -->



		<?php 
			echo '<pre>';
			$getallmeta=get_post_meta( $post->ID); print_r($getallmeta);
			echo '</pre>';
		?>
