<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'artsv3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a>Et7m:t|F{ULM|Q#-9sQ5sxlnpk4wr}kO3g)2I1TkP PYoe-zJ*x;ShZ-_G2o2 ');
define('SECURE_AUTH_KEY',  'I8F9Uv++yKXYWYxA)l@X]K<9qr!MZU#]7u0ov}mv+neO!GIte~Z0qoJX,(1)qwt(');
define('LOGGED_IN_KEY',    'zN_4nuF  -4{mxh@f>f9/K1;A)$5_/akCl?ADDG=k~cy 6Pjnq=9hNj=ZD+RN,^1');
define('NONCE_KEY',        'Ce.<V1J^vzG?^6$sU{@7$-4aTzs-4)i!-1#zyXFn:sZ h7-o=nTT-bhmQ>bM$l;$');
define('AUTH_SALT',        '=ST=Hswr|iB ,p+vWq- 1%TMi6e&[G}=jDTKR,fru@I;!N=3ZRZ&J(5-B-VGy&84');
define('SECURE_AUTH_SALT', 'x_H45|fv,QOHzTA3.UWIxJA} ^?f!rM}a.F8E 0!f6nIkm&~,+&=!?7]J70W$,hu');
define('LOGGED_IN_SALT',   'qKs;N6w#m##wXxG}f=*t,[MW>M@a5uu${rv+2!0Xj*ZGw|O<kk0L5^^,e>ag3!?V');
define('NONCE_SALT',       '~27=.*r:L.4zj2FUnqM~3+`<d+pLR:[up-F]g~!In@[?x{Ml?,XxEW7&0v1r-J+-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
